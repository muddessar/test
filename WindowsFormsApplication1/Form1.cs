﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using Microsoft.TeamFoundation;
using Microsoft.TeamFoundation.Client; 
using Microsoft.TeamFoundation.Framework.Common;
using Microsoft.TeamFoundation.Framework.Client;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ConnectTFS_Click(object sender, EventArgs e)
        {
            // Connect to Team Foundation Server
            //    server is the name of the server that is running the Team Foundation application-tier.
            //    port is the port that Team Foundation uses. The default is port is 8080.
            //    vpath is the virutal path to the Team Foundation application. The default path is tfs.
            //TfsConfigurationServer configurationServer = TfsConfigurationServerFactory.GetConfigurationServer(new Uri("http://eagle:8080/tfs"));
            // Get the catalog of team project collections
            //ReadOnlyCollection<CatalogNode> collectionNodes = configurationServer.CatalogNode.QueryChildren(new[] { CatalogResourceTypes.ProjectCollection },false, CatalogQueryOptions.None);
            TeamProjectPicker tpp = new TeamProjectPicker(TeamProjectPickerMode.MultiProject, false);
            DialogResult result = tpp.ShowDialog();
            if (result == DialogResult.OK)
            {
                listBox1.Items.Add(tpp.SelectedTeamProjectCollection.Uri);
                listBox1.Items.Add(tpp.SelectedProjects[0].ToString());
                var workItemStore = new WorkItemStore(tpc);
            }

            // List the team project collections
            /*foreach (CatalogNode collectionNode in collectionNodes)
            {
                // Use the InstanceId property to get the team project collection
                Guid collectionId = new Guid(collectionNode.Resource.Properties["InstanceId"]);
                TfsTeamProjectCollection teamProjectCollection = configurationServer.GetTeamProjectCollection(collectionId);

                // Print the name of the team project collection
                //Console.WriteLine("Collection: " + teamProjectCollection.Name);
                listBox1.Items.Add(teamProjectCollection.Name);

                // Get a catalog of team projects for the collection
                ReadOnlyCollection<CatalogNode> projectNodes = collectionNode.QueryChildren(
                    new[] { CatalogResourceTypes.TeamProject },
                    false, CatalogQueryOptions.None);

                // List the team projects in the collection
                foreach (CatalogNode projectNode in projectNodes)
                    
                {
                    listBox1.Items.Add(projectNode.Resource.DisplayName);
                    //Console.WriteLine(" Team Project: " + projectNode.Resource.DisplayName);
                }

            }*/

        }

        
    }
}
